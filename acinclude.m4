
AC_DEFUN([AC_HEADER_FIND],
[
	r="0"
	for dir in $2; do
		AC_MSG_CHECKING(for $1 in $dir)
		if test -r $dir/$1; then
			CPPFLAGS="$CPPFLAGS -I$dir"
			AC_SUBST(CPPFLAGS)
			AC_DEFINE($3)
			AC_MSG_RESULT(yes)
			r="1"
			break;
		fi
		AC_MSG_RESULT(no)
	done
	if test $r = 0; then
		AC_MSG_WARN("$1 not found")
	fi
]
)

# MD_PATH(PACKAGE, HEADER, LIBRARY, SYMBOL)
# -----------------------------------------
# Set PACKAGE_CPPFLAGS and PACKAGE_LIBS to the flags needed to
# compile/link with PACKAGE. HEADER and LIBRARY should be a header
# file and a library, respectively, belonging to PACKAGE. SYMBOL
# should be a SYMBOL in LIBRARY. If the package is available, define
# HAVE_PACKAGE.
AC_DEFUN([MD_PATH],
[AS_VAR_PUSHDEF([md_Package], [md_path_have_$1])dnl
AS_VAR_PUSHDEF([md_Inc], [md_path_include_$1])dnl
AS_VAR_PUSHDEF([md_Lib], [md_path_lib_$1])dnl
AS_VAR_SET(md_Package, yes)
AS_VAR_SET(md_Inc, [])
AS_VAR_SET(md_Lib, [])
AC_ARG_WITH([$1],
             AC_HELP_STRING([--with-$1=DIR], [search $1 header files in DIR/include, library in DIR/lib]),
[case $withval in
yes)
  AS_VAR_SET(md_Package, yes)
  ;;
no)
  AS_VAR_SET(md_Package, no)
  ;;
*)
  AS_VAR_SET(md_Package, yes)
  AS_VAR_SET(md_Inc, ["$withval/include"])
  AS_VAR_SET(md_Lib, ["$withval/lib"])
  ;;
esac])
AC_ARG_WITH([$1-include],
             AC_HELP_STRING([--with-$1-include=DIR], [search $1 header files in DIR]),
[case $withval in
yes)
  AS_VAR_SET(md_Package, yes)
  ;;
no)
  AS_VAR_SET(md_Package, no)
  ;;
*)
  AS_VAR_SET(md_Package, yes)
  AS_VAR_SET(md_Inc, ["$withval"])
  ;;
esac])
AC_ARG_WITH([$1-lib],
             AC_HELP_STRING([--with-$1-lib=DIR], [search $1 library in DIR]),
[case $withval in
yes)
  AS_VAR_SET(md_Package, yes)
  ;;
no)
  AS_VAR_SET(md_Package, no)
  ;;
*)
  AS_VAR_SET(md_Package, yes)
  AS_VAR_SET(md_Lib, ["$withval"])
  ;;
esac])
AS_TR_CPP([$1]_CPPFLAGS)= AS_TR_CPP([$1]_LIBS)=
AS_IF([test AS_VAR_GET(md_Package) = yes],
[AS_IF([test "x[]AS_VAR_GET(md_Inc)" != x], [AS_TR_CPP([$1]_CPPFLAGS)="-I[]AS_VAR_GET(md_Inc)"])
AS_IF([test "x[]AS_VAR_GET(md_Lib)" != x], [AS_TR_CPP([$1]_LIBS)="-L[]AS_VAR_GET(md_Lib)"])
save_CPPFLAGS="$CPPFLAGS"; CPPFLAGS="$CPPFLAGS $[]AS_TR_CPP([$1]_CPPFLAGS)"
AC_CHECK_HEADER([$2],
[save_LDFLAGS="$LDFLAGS"; LDFLAGS="$LDFLAGS $[]AS_TR_CPP([$1]_LIBS)"
AC_CHECK_LIB([$3], [$4], [AS_TR_CPP([$1]_LIBS)="$[]AS_TR_CPP([$1]_LIBS) -l[$3]"], [AS_VAR_SET(md_Package, no)])
LDFLAGS="$save_LDFLAGS"], [AS_VAR_SET(md_Package, no)])
CPPFLAGS="$save_CPPFLAGS"])
AH_TEMPLATE(AS_TR_CPP(HAVE_[$1]), [Define if package `$1' is available.])dnl
AS_IF([test AS_VAR_GET(md_Package) = yes], [AC_DEFINE(AS_TR_CPP(HAVE_[$1]), [1])])dnl
AS_VAR_POPDEF([md_Lib])dnl
AS_VAR_POPDEF([md_Inc])dnl
AS_VAR_POPDEF([md_Package])dnl
])# MD_PATH
