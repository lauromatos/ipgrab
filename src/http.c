/**************************************************************************** 
** File: http.c
**
** Author: Mike Borella
**
** Comments: Dump HTTP header information. I didn't try to do anything
** fancy with this - I just dump the plaintext headers.  This makes 
** debugging easier since parsing is such a pain to get right.
**
** $Id: http.c,v 1.6 2001/05/28 19:24:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "http.h"

#define LINE_SIZE 256
#define FALSE 0
#define TRUE  1

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_http()
**
** Parse HTTP packet and dump fields. 
**
**----------------------------------------------------------------------------
*/

void dump_http(packet_t *pkt)
{
  char line[LINE_SIZE];
  int i;
  int len;
  int first_line = 1;

#ifdef DEBUG
  printf("\nEntering HTTP\n");
#endif

  /* Set the layer */
  set_layer(LAYER_APPLICATION);

  /* announcement */
  if (!my_args->m)
    display_header_banner("HTTP");
  
  while(1)
    {
      len = get_packet_line(line, LINE_SIZE, pkt);
      if (len == 0)
	break;
      
      /*
       * If the first line doesn't 
       * contain 'HTTP/', assume that there are no headers.
       * In minimal mode, just dump the first line.
       */

      if (first_line)
	{
	  if (!strstr(line, "HTTP/"))
	    break;
	  if (my_args->m)
	    {
	      display_minimal_string(line);
	      break;
	    }
	}
      first_line = 0;

      /*
       * If we have a line of all whitespace, that's the end of the headers.
       */

      if (isspace_str(line, strlen(line)))
	break;
	  
      /*
       * Display a header
       */

      display_string("Header", line);

      /*
       * Convert each line to lower case for easier parsing.
       */

      for (i=0; i<strlen(line); i++)
	line[i] = tolower(line[i]);
    }

  /* dump the hex buffer */
  hexbuffer_flush();

#ifdef DEBUG
  printf("\nLeaving HTTP\n");
#endif

}
