/**************************************************************************** 
** File: ipxrip.h
**
** Author: Mike Borella
**
** Comments: IPX/RIP header format and such
**
** $Id: ipxrip.h,v 1.3 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef IPXRIP_H
#define IPXRIP_H

#include "global.h"
#include "local.h"

/*
 * IPX RIP commands 
 */

#define IPXRIP_COMMAND_REQUEST    1
#define IPXRIP_COMMAND_RESPONSE   2

/*
 * Routing entries
 */

typedef struct ipxrip_entry
{
  u_int32_t net;
  u_int16_t hops;
  u_int16_t ticks;
} ipxrip_entry_t;

/*
 * Generic header
 */

typedef struct ipxrip_header
{
  u_int16_t op;
} ipxrip_header_t;

void dump_ipxrip(packet_t *);

#endif
