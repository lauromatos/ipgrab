/**************************************************************************** 
 **
 ** File: rsip.h
 **
 ** Author: Mike Borella
 **
 ** Comments: RSIP header info
 **
 *****************************************************************************/

/*
 * RSIP port number
 */

#define RSIP_PORT 4455

void dump_rsip(u_char *bp, int length);

