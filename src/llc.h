/**************************************************************************** 
** File: llc.h
**
** Author: Mike Borella
**
** Comments: LLC encapsulation for ethernet packets
**
** $Id: llc.h,v 1.4 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef LLC_H
#define LLC_H

#include "global.h"
#include "local.h"

/*
 * LLC header
 */

typedef struct llc_header
{
  u_int8_t  dsap;
  u_int8_t  ssap;
} llc_header_t;

void dump_llc(packet_t *);

#endif



