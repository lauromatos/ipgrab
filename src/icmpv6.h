/**************************************************************************** 
** File: icmpv6.h
**
** Author: Mike Borella
**
** Generic ICMPv6 header structures.
**
** $Id: icmpv6.h,v 1.8 2006/11/21 07:47:34 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef ICMPV6_H
#define ICMPV6_H

#include "global.h"
#include "local.h"

/*
 * ICMPv6 type fields
 */

#define ICMPV6_TYPE_DESTUNREACHABLE     1
#define ICMPV6_TYPE_PACKETTOOBIG        2
#define ICMPV6_TYPE_TIMEEXCEEDED        3
#define ICMPV6_TYPE_PARAMETERPROBLEM    4
#define ICMPV6_TYPE_ECHOREQUEST         128
#define ICMPV6_TYPE_ECHOREPLY           129
#define ICMPV6_TYPE_GROUPMEMQUERY       130
#define ICMPV6_TYPE_GROUPMEMREPORT      131
#define ICMPV6_TYPE_GROUPMEMREDUCTION   132
#define ICMPV6_TYPE_ROUTERSOLICIT       133
#define ICMPV6_TYPE_ROUTERADVERT        134
#define ICMPV6_TYPE_NEIGHBORSOLICIT     135
#define ICMPV6_TYPE_NEIGHBORADVERT      136
#define ICMPV6_TYPE_REDIRECT            137

/*
 * ICMPv6 destination unreachable code fields
 */

#define ICMPV6_DUCODE_NOROUTE           0
#define ICMPV6_DUCODE_ADMIN             1
#define ICMPV6_DUCODE_ADDRUNREACHABLE   2
#define ICMPV6_DUCODE_PORTUNREACHABLE   3

/*
 * ICMPv6 time exceeded code fields
 */

#define ICMPV6_TECODE_HOPLIMIT          0
#define ICMPV6_TECODE_REASSEMBLY        1

/*
 * ICMPv6 parameter problem code fields
 */

#define ICMPV6_PPCODE_BADHEADERFIELD    0
#define ICMPV6_PPCODE_BADNEXTHEADER     1
#define ICMPV6_PPCODE_BADIPV6OPTION     2

/*
 * ICMPv6 common header format
 */

typedef struct icmpv6_header
{
  u_int8_t       type;
  u_int8_t       code;
  u_int16_t      checksum;
} icmpv6_header_t;

void dump_icmpv6(packet_t *);

#endif
