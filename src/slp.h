/**************************************************************************** 
**
** File: slp.h 
**
** Author: Mike Borella
**
** $Id: slp.h,v 1.3 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef SLP_H
#define SLP_H

#include "global.h"
#include "local.h"

#define SLP_MSGTYPE_SRVRQST       1
#define SLP_MSGTYPE_SRVRPLY       2
#define SLP_MSGTYPE_SRVREG        3
#define SLP_MSGTYPE_SRVDEREG      4
#define SLP_MSGTYPE_SRVACK        5
#define SLP_MSGTYPE_ATTRRQST      6
#define SLP_MSGTYPE_ATTRRPLY      7
#define SLP_MSGTYPE_DAADVERT      8
#define SLP_MSGTYPE_SRVTYPERQST   9
#define SLP_MSGTYPE_SRVTYPERPLY   10
#define SLP_MSGTYPE_SAADVERT      11

/*
 * Fixed portion of SLPv1 header 
 */

typedef struct slpv1_header
{
  u_int8_t      version;
  u_int8_t      function_id;
  u_int16_t     length;
  u_int8_t      flags_rsrvd;
  u_int8_t      dialect;
  u_int16_t     lang_code;
  u_int16_t     char_encoding;
  u_int16_t     xid;
} slpv1_header_t;

/*
 * Fixed portion of SLPv2 header 
 */

typedef struct slpv2_header
{
  u_int8_t      version;
  u_int8_t      function_id;
  u_int8_t      length[3];
  u_int16_t     flags_rsrvd;
  u_int8_t      next_ext_offset[3];
  u_int16_t     xid;
  u_int16_t     lang_tag_length;
} slpv2_header_t;

void dump_slp(packet_t *);

#endif
