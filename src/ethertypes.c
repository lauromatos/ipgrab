/**************************************************************************** 
** File: ethertypes.c
**
** Author: Mike Borella
**
** Comments: Functions to handle ethernet types
**
** $Id: ethertypes.c,v 1.6 2001/11/02 00:23:06 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "ethertypes.h"

/*
 * Ethernet type map
 */

strmap_t ethertype_map[] =
{
  { ETHERTYPE_IP,      "IP" },
  { ETHERTYPE_ARP,     "ARP" },
  { ETHERTYPE_RARP,    "RARP" },
  { ETHERTYPE_IPX,     "IPX" },
  { ETHERTYPE_IPV6,    "IPV6" },
  { ETHERTYPE_PPP,     "PPP" },
  { ETHERTYPE_PPPOED,  "PPPoEd" },
  { ETHERTYPE_PPPOES,  "PPPoEs" },
  { ETHERTYPE_PPPHDLC, "PPP with HDLC framing" },
  { 0, "" }
};

/*----------------------------------------------------------------------------
**
** ethertype2func()
**
** Map an ethertype to a function that handles that type
**
**----------------------------------------------------------------------------
*/

ethertype_func_t ethertype2func(u_int16_t et)
{
  ethertype_func_t f;

  switch(et)
    {
    case ETHERTYPE_IP:
      f = dump_ip;
      break;
      
    case ETHERTYPE_ARP:
    case ETHERTYPE_RARP:
      f = dump_arp;
      break;

    case ETHERTYPE_IPX:
      f = dump_ipx;
      break;

    case ETHERTYPE_IPV6:
      f = dump_ipv6;
      break;

    case ETHERTYPE_PPP:
      f = dump_ppp;
      break;

    case ETHERTYPE_PPPHDLC:
      f = dump_ppp_hdlc;
      break;

    case ETHERTYPE_PPPOED:
      f = dump_pppoed;
      break;

    case ETHERTYPE_PPPOES:
      f = dump_pppoes;
      break;

    default:
      f = NULL;
      break;
    }

  return f;
}
