/**************************************************************************** 
** File: hexbuffer.h
**
** Author: Mike Borella
**
** Header file for hex buffer manipulation.  The hex buffer holds
** all of the bytes read since the last time the hex buffer was flushed.
** When the hex buffer is flushed, all bytes are displayed in hex and 
** (printable) ascii format to the screen, preceeded by a dotted line.
**
** $Id: hexbuffer.h,v 1.3 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef HEXBUFFER_H
#define HEXBUFFER_H

#include "global.h"
#include "local.h"

inline void hexbuffer_add(u_int8_t *ptr, int length);
inline void hexbuffer_flush(void);
inline void hexbuffer_kill(void);
#endif
