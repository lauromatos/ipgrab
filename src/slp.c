/**************************************************************************** 
** File: sip.c
**
** Author: Mike Borella
**
** Comments: Dump SLP header.  More parsing later
**
** $Id: slp.c,v 1.5 2002/01/03 00:04:01 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "slp.h"

#define HOLDER_SIZE 64

strmap_t slp_msgtype_map[] =
{
  { SLP_MSGTYPE_SRVRQST,      "service request" },
  { SLP_MSGTYPE_SRVRPLY,      "service reply" },
  { SLP_MSGTYPE_SRVREG,       "service registration" },
  { SLP_MSGTYPE_SRVDEREG,     "service deregistration" },
  { SLP_MSGTYPE_SRVACK,       "service acknowledge" },
  { SLP_MSGTYPE_ATTRRQST,     "attribute request" },  
  { SLP_MSGTYPE_ATTRRPLY,     "attribute reply" },  
  { SLP_MSGTYPE_DAADVERT,     "DA advertisement" },  
  { SLP_MSGTYPE_SRVTYPERQST,  "service type request" },  
  { SLP_MSGTYPE_SRVTYPERPLY,  "service type reply" },  
  { SLP_MSGTYPE_SAADVERT,     "SA advertisement" },  
  { 0, "" }
};

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_slp()
**
** Parse SLP packet and dump fields.
**
**----------------------------------------------------------------------------
*/

void dump_slp(packet_t *pkt)
{
  slpv1_header_t slpv1;
  slpv2_header_t slpv2;
  char holder[HOLDER_SIZE];
  u_int8_t ver;

  /* Set the layer */
  set_layer(LAYER_APPLICATION);

  /*
   * Look at the first byte to determine the version
   */

  if (look_packet_bytes((u_int8_t *) &ver, pkt, 1) == 0)
    return;
  switch(ver)
    {

    case 1:
      /* Get the header */
      if (get_packet_bytes((u_int8_t *) &slpv1, pkt, 12) == 0)
	return;

      /* Conversions */
      slpv1.length = ntohs(slpv1.length);
      slpv1.char_encoding = ntohs(slpv1.char_encoding);
      slpv1.xid = ntohs(slpv1.xid);
      
      if (my_args->m)
	{
	  /* In minimal mode lets just dump the version and operation */
	  display_minimal_string("| SLPv1 ");
	  display_minimal_string(map2str(slp_msgtype_map, slpv1.function_id));
	  display_minimal_string(" ");
	}
      else
	{
	  /* announcement */
	  display_header_banner("SLPv1 Header");
	  
	  /* dump the version and operation */
	  display("Version", (u_int8_t *) &slpv1.version, 1, DISP_DEC);
	  snprintf(holder, HOLDER_SIZE, "%d (%s)", slpv1.function_id,
		   map2str(slp_msgtype_map, slpv1.function_id));
	  display_string("Operation", holder);
	  display("Length", (u_int8_t *) &slpv1.length, 2, DISP_DEC);
	  display("Flags/Reserved", (u_int8_t *) &slpv1.flags_rsrvd, 1, 
		  DISP_HEX);
	  display("Dialect", (u_int8_t *) &slpv1.dialect, 1, DISP_DEC);
	  display("Language code", (u_int8_t *) &slpv1.lang_code, 2, 
		  DISP_STRING);
	  display("Character encoding", (u_int8_t *) &slpv1.char_encoding, 
		  2, DISP_DEC);
	  display("XID", (u_int8_t *) &slpv1.xid, 2, DISP_DEC);
	}
      break;

    case 2:
      /* Get the header */
      if (get_packet_bytes((u_int8_t *) &slpv2, pkt, 14) == 0)
	return;
      
      /* Conversions */
      reverse_byte_order((u_int8_t *) &slpv2.length, 3);
      slpv2.flags_rsrvd = ntohs(slpv2.flags_rsrvd);
      reverse_byte_order((u_int8_t *) &slpv2.next_ext_offset, 3);
      slpv2.xid = ntohs(slpv2.xid);
      slpv2.lang_tag_length = ntohs(slpv2.lang_tag_length);
      
      if (my_args->m)
	{
	  /* In minimal mode lets just dump the version and operation */
	  display_minimal_string("| SLPv2 ");
	  display_minimal_string(map2str(slp_msgtype_map, slpv2.function_id));
	  display_minimal_string(" ");
	}
      else
	{
	  /* announcement */
	  display_header_banner("SLPv2 Header");
	  
	  /* dump the version and operation */
	  display("Version", (u_int8_t *) &slpv2.version, 1, DISP_DEC);
	  snprintf(holder, HOLDER_SIZE, "%d (%s)", slpv1.function_id,
		   map2str(slp_msgtype_map, slpv1.function_id));
	  display_string("Operation", holder);
	  display("Length", (u_int8_t *) &slpv2.length, 3, DISP_DEC);
	  display("Flags/Reserved", (u_int8_t *) &slpv2.flags_rsrvd, 2, 
		  DISP_HEX);
	  display("Next extension offset", 
		  (u_int8_t *) &slpv2.next_ext_offset, 3, DISP_DEC);
	  display("XID", (u_int8_t *) &slpv2.xid, 2, DISP_DEC);
	  display("Language tag length", (u_int8_t *) &slpv2.lang_tag_length,
		  2, DISP_DEC);
	}
      
    default:
      break;
    }
}

