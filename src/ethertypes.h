/**************************************************************************** 
** File: ethertypes.h
**
** Author: Mike Borella
**
** Comments: Header file for ethernet types
**
** $Id: ethertypes.h,v 1.5 2001/03/28 18:34:42 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef ETHERTYPES_H
#define ETHERTYPES_H

#include "global.h"
#include "ip.h"
#include "ipv6.h"
#include "arp.h"
#include "ipx.h"
#include "ppp.h"
#include "pppoe.h"

#define ETHERTYPE_IP        0x0800  /* IPv4 */
#define ETHERTYPE_ARP       0x0806  /* Addr. resolution protocol */
#define ETHERTYPE_RARP      0x8035  /* Reverse addr. resolution protocol */
#define ETHERTYPE_IPX       0x8137  /* IPX family */
#define ETHERTYPE_IPV6      0x86dd  /* IPv6 */
#define ETHERTYPE_PPP       0x880b  /* PPP */
#define ETHERTYPE_PPPOED    0x8863  /* PPPoE discovery */
#define ETHERTYPE_PPPOES    0x8864  /* PPPoE session */
#define ETHERTYPE_PPPHDLC   0x8881  /* CDMA2000 PPP with HDLC framing in GRE */

/*
 * Define a type that is a function that takes a packet_t pointer and 
 * returns a void 
 */

typedef void (*ethertype_func_t)(packet_t *);

/*
 * This function maps a ethertype to a function that processes the 
 * protocol that runs on that ethertype
 */

ethertype_func_t ethertype2func(u_int16_t);


#endif



