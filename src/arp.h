/**************************************************************************** 
** File: arp.h
**
** Author: Mike Borella
**
** Comments: Generic ARP structure - an attempt at OS independence
**
** $Id: arp.h,v 1.3 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef ARP_H
#define ARP_H

#include "global.h"
#include "local.h"

/*
 * ARP hardware identifiers (not even close to complete, but sufficient)
 */

#define ARP_HWTYPE_ETHERNET  1
#define ARP_HWTYPE_IEEE802   6
#define ARP_HWTYPE_IEEE1394  29

/* 
 * ARP protocol opcodes. 
 */

#define ARP_OPCODE_REQUEST   1            /* ARP request                  */
#define ARP_OPCODE_REPLY     2            /* ARP reply                    */
#define RARP_OPCODE_REQUEST  3            /* RARP request                 */
#define RARP_OPCODE_REPLY    4            /* RARP reply                   */

/*
 * ARP header
 */

typedef struct arp_header
{
  u_int16_t hwaddr_format;             /* format of hardware address   */
  u_int16_t protoaddr_format;          /* format of protocol address   */
  u_int8_t  hwaddr_length;             /* length of hardware address   */
  u_int8_t  protoaddr_length;          /* length of protocol address   */
  u_int16_t opcode;                    /* ARP opcode (command)         */
} arp_header_t;

/*
 * Ethernet ARP format
 */
    
typedef struct ether_arp
{
  u_int8_t sender_hwaddr[6];        /* sender hardware address */
  u_int8_t sender_protoaddr[4];     /* sender protocol address */
  u_int8_t target_hwaddr[6];        /* target hardware address */
  u_int8_t target_protoaddr[4];     /* target protocol address */
} ether_arp_t;


void dump_arp(packet_t *);

#endif
