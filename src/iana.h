/**************************************************************************** 
** File: iana.h
**
** Author: Mike Borella
**
** Comments: IANA specific definitions
**
** $Id: iana.h,v 1.1 2001/11/21 18:04:12 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef IANA_H
#define IANA_H

#include "global.h"
#include "local.h"

/*
 * List of enterprise numbers (e.g., vendor ID's).  Not even close to 
 * complete.
 */


#define IANA_ENTERPRISE_CISCO             9
#define IANA_ENTERPRISE_3COM              43
#define IANA_ENTERPRISE_3COMCARRIER       429
#define IANA_ENTERPRISE_3GPP2             5535
#define IANA_ENTERPRISE_COMMWORKS         10890


#endif

