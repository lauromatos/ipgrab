/**************************************************************************** 
** File: ethernet.h
**
** Author: Mike Borella
**
** Comments: Generic ethernet structure - an attempt at OS independence
**
** $Id: ethernet.h,v 1.5 2000/08/31 20:28:25 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef ETHERNET_H
#define ETHERNET_H

#include "global.h"
#include "local.h"

#define ETHERMTU                1500

/*
 * Ethernet header
 */

typedef struct ether_header
{
  u_int8_t  dst[6];
  u_int8_t  src[6];
  u_int16_t type;
} ether_header_t;

void dump_ethernet(packet_t *);

#endif



