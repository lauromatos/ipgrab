/**************************************************************************** 
** File: netbios_ns.h
**
** Author: Mike Borella
**
** Definitions for NETBIOS name service routines
**
** $Id: netbios_ns.h,v 1.5 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef NETBIOS_NS_H
#define NETBIOS_NS_H

#include "global.h"
#include "local.h"

/*
 * R flag values 
 */

#define NETBIOS_NS_R_QUERY    0
#define NETBIOS_NS_R_RESPONSE 1

/*
 * Opcode values
 */

#define NETBIOS_NS_OPCODE_QUERY        0
#define NETBIOS_NS_OPCODE_REGISTRATION 5
#define NETBIOS_NS_OPCODE_RELEASE      6
#define NETBIOS_NS_OPCODE_WACK         7
#define NETBIOS_NS_OPCODE_REFRESH      8

/*
 * Rcode values
 */

#define NETBIOS_NS_RCODE_NOERROR       0

/*
 * Query types
 */

#define NETBIOS_NS_QUERYTYPE_IPADDR      0x0001
#define NETBIOS_NS_QUERYTYPE_NAMESERVER  0x0002
#define NETBIOS_NS_QUERYTYPE_NULL        0x000A
#define NETBIOS_NS_QUERYTYPE_GENERAL     0x0020
#define NETBIOS_NS_QUERYTYPE_NODESTATUS  0x0021

/* 
 * Query classes
 */

#define NETBIOS_NS_QUERYCLASS_IP  1

/*
 * Format of netbios name service common header 
 *
 *                       1 1 1 1 1 1 1 1 1 1 2 2 2 2 2 2 2 2 2 2 3 3
 *   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  |         NAME_TRN_ID           | OPCODE  |   NM_FLAGS  | RCODE |
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  |          QDCOUNT              |           ANCOUNT             |
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *  |          NSCOUNT              |           ARCOUNT             |
 *  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

typedef struct netbios_ns
{
  u_int16_t transaction_id;
#if defined(WORDS_BIGENDIAN)
  u_int8_t  r:1, 
            opcode:4,
            flags_high:3;
  u_int8_t  flags_low:4,
            rcode:4;
#else
  u_int8_t  flags_high:3, 
            opcode:4,
            r:1;
  u_int8_t  rcode:4,
            flags_low:4;
#endif
  u_int16_t questions;
  u_int16_t answers;
  u_int16_t auth_answers;
  u_int16_t add_answers;
} netbios_ns_t;

void dump_netbios_ns(packet_t *);

#endif
