/**************************************************************************** 
** File: igmp.c
**
** Author: Mike Borella
**
** Comments: Dump IGMP information
**
** $Id: igmp.c,v 1.13 2006/11/21 07:47:34 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "igmp.h"

#define HOLDER_SIZE 64

extern struct arg_t * my_args;

/* 
 * Contains the descriptions of IGMP types 
 */

strmap_t igmp_type_map[] = 
{
  { IGMP_TYPE_MEMBERQUERY,     "membership query" },
  { IGMP_TYPE_MEMBERREPORTV1,  "v1 membership report" },
  { IGMP_TYPE_DVMRP,           "DVMRP" },
  { IGMP_TYPE_MEMBERREPORTV2,  "v2 membership report" },
  { IGMP_TYPE_LEAVEGROUP,      "leave group" },
  { 0, "" }
};

/*----------------------------------------------------------------------------
**
** dump_igmp()
**
** Parse IGMP header and dump fields
**
**----------------------------------------------------------------------------
*/

void dump_igmp(packet_t *pkt)
{
  igmp_header_t igmp;
  char holder[HOLDER_SIZE];

  /* Set the layer */
  set_layer(LAYER_NETWORK);

  /*
   * Stats accounting
   */

  stats_update(STATS_IGMP);

  /*
   * Get the header. Note that the length of 8 may cause a problem with 
   * some DVMRP packets, but so far it hasn't.  Change it to 4 later for 
   * safety... -MB
   */

  if (get_packet_bytes((u_int8_t *) &igmp, pkt, 8) == 0)
    return;

  /*
   * Conversions
   */

  igmp.checksum = ntohs(igmp.checksum);

  /*
   * Dump the contents
   */

  if (my_args->m)
    {
      /* In minimal mode lets just dump the type and address */
      display_minimal_string("| IGMP ");
      display_minimal_string(map2str(igmp_type_map, igmp.type));
      display_minimal_string(" ");
    }
  else
    { 
      /* announcement */
      display_header_banner("IGMP Header");
      
      /* Dump the type, max resp time, checksum and address */
      snprintf(holder, HOLDER_SIZE, "%d (%s)", igmp.type, 
	      map2str(igmp_type_map, igmp.type));
      display_string("Type", holder);
      display("Max response time", (u_int8_t *) &igmp.max_resp_time, 1,
	      DISP_DEC);
      display("Checksum", (u_int8_t *) &igmp.checksum, 2, DISP_DEC);
      
      /*
       * We currently don't try to interpret DVMRP packets...
       */
      
      if (igmp.type != IGMP_TYPE_DVMRP)
	display_ipv4("Group address", (u_int8_t *) &igmp.address);
    }
}


