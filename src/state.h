/**************************************************************************** 
**
** File: state.h
**
** Author: Mike Borella
**
** Header Definitions for packet level state
**
** $Id: state.h,v 1.1 2001/10/09 23:20:42 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef STATE_H
#define STATE_H

#include "global.h"

/*
 * Function prototypes
 */

void           state_set_srcaddr(u_int32_t);
void           state_set_dstaddr(u_int32_t);
void           state_set_srcport(u_int16_t);
void           state_set_dstport(u_int16_t);
u_int32_t      state_get_srcaddr(void);
u_int32_t      state_get_dstaddr(void);
u_int16_t      state_get_srcport(void);
u_int16_t      state_get_dstport(void);

#endif
