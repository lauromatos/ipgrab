/**************************************************************************** 
** File: rsvp.h
**
** Author: Mike Borella
**
** Comments: 
**
** $Id: rsvp.h,v 1.1 2001/03/26 20:55:24 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef RSVP_H
#define RSVP_H

#include "global.h"
#include "local.h"

/*
 * RSVP common header 
 */

typedef struct rsvp_header
{
#if defined(WORDS_BIGENDIAN)
  u_int8_t   version:4,
             flags:4;
#else
  u_int8_t   flags:4,
             version:4;
#endif
  u_int8_t   msg_type;
  u_int16_t  checksum;
  u_int8_t   send_ttl;
  u_int8_t   reserved;
  u_int16_t  length;
} rsvp_header_t;

void dump_rsvp(packet_t *);

#endif
