/**************************************************************************** 
** File: payload.c
**
** Author: Mike Borella
**
** Comments: Dump packet payload
**
** $Id: payload.c,v 1.9 2001/11/15 00:22:36 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "payload.h"

#define PAYLOAD_SIZE  1500

extern struct arg_t * my_args;

/*----------------------------------------------------------------------------
**
** dump_payload()
**
** Dump printable portions of packet payload
**
**----------------------------------------------------------------------------
*/

void dump_payload(packet_t *pkt)
{
  char   holder[PAYLOAD_SIZE];
  int    bytes;

  /* Quit if we're in minimal mode */
  if (my_args->m)
    return;

  /* Set the layer */
  set_layer(LAYER_APPLICATION);

  /* display announcement */
  display_header_banner("Payload");

  /* The rest of the packet is payload, so lets grab the whole thing */
  bytes = get_packet_apparentbytesleft(pkt);
  if (bytes <= 0) 
    return;
  if (get_packet_bytes(holder, pkt, bytes) == 0)
    return;

  /*
   * Display the hex and text
   */

  dump_hex_and_text(holder, bytes);

  return;
  
}
