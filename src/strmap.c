/**************************************************************************** 
** File: strmap.c
**
** Author: Mike Borella
**
** Comments: Map IANA numbers to string
**
** $Id: strmap.c,v 1.6 2006/11/21 07:47:34 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "strmap.h"

/*----------------------------------------------------------------------------
**
** map2str()
**
** Convert an IANA protocol number to a descriptive string.  This process
** is slow, so maybe we shouldn't do it in "fast mode"...whatever that may 
** become.
**
** Returns the target string, or "unknown" if we don't find the target.
**
**----------------------------------------------------------------------------
*/

inline char * map2str(strmap_t *sm, u_int32_t index)
{
  int i = 0;

  while(strlen(sm[i].string) != 0)
    {
      if (sm[i].number == index)
	return sm[i].string;
      i++;
    }
  
  return "unknown";
}
