/**************************************************************************** 
** File: ip_protocols.c
**
** Author: Mike Borella
**
** Comments: Functions to handle certain IP protocol numbers
**
** $Id: ip_protocols.c,v 1.18 2001/11/02 00:23:06 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "ip_protocols.h"

#define IP_PROTO_ARRAY_SIZE 256

/*
 * Make an array that will hold all of the pointers to protocol functions 
 */

void (*ip_proto_func[IP_PROTO_ARRAY_SIZE])(packet_t *);

/*
 * Fill the array up with pointers.  I'd like to do this in global space
 * so that this function does not have to be explicitly called...but I can't
 * get that to work right (won't compile).  Help?
 */

void init_ip_protocols(void)
{
  int i;

  for (i = 0; i < 256; i++)
    ip_proto_func[i] = NULL;

  ip_proto_func[PROTO_ICMP]     = dump_icmp;
  ip_proto_func[PROTO_IGMP]     = dump_igmp;
  ip_proto_func[PROTO_IPENCAP]  = dump_ip;
  ip_proto_func[PROTO_TCP]      = dump_tcp;
  ip_proto_func[PROTO_UDP]      = dump_udp;
  ip_proto_func[PROTO_GRE]      = dump_gre;
  ip_proto_func[PROTO_RSVP]     = dump_rsvp;
  ip_proto_func[PROTO_ESP]      = dump_esp;
  ip_proto_func[PROTO_AH]       = dump_ah;
  ip_proto_func[PROTO_OSPF]     = dump_ospf;
  ip_proto_func[PROTO_IPV6ICMP] = dump_icmpv6;
  ip_proto_func[PROTO_IPV6]     = dump_ipv6;
  
  ip_proto_func[PROTO_PUP] = dump_tcp;
}

/*
 * IP protocol map.  There appears to be 3 different types of IP in IP 
 * encapsulation.  Weird.  I'm not sure which is(are) really used...
 */

strmap_t ipproto_map[] =
{
  { PROTO_IPV6HOP,         "IPv6 hop by hop" },
  { PROTO_ICMP,            "ICMP" },
  { PROTO_IGMP,            "IGMP" },
  { PROTO_GGP,             "GGP" },
  { PROTO_IPENCAP,         "IP encapsulation" },
  { PROTO_ST,              "ST" },
  { PROTO_TCP,             "TCP" },
  { PROTO_CBT,             "CBT" },
  { PROTO_EGP,             "EGP" },
  { PROTO_IGP,             "IGP" },
  { PROTO_PUP,             "PUP" },
  { PROTO_UDP,             "UDP" },
  { PROTO_HMP,             "HMP" },
  { PROTO_XNSIDP,          "XNS IDP" },
  { PROTO_RDP,             "RDP" },
  { PROTO_IPV6,            "IPv6" },
  { PROTO_IPV6ROUTE,       "IPv6 route" },
  { PROTO_IPV6FRAG,        "IPv6 fragmentation" },
  { PROTO_IDRP,            "IDRP" },
  { PROTO_RSVP,            "RSVP" },
  { PROTO_GRE,             "GRE" },
  { PROTO_ESP,             "ESP" },
  { PROTO_AH,              "AH" },
  { PROTO_NARP,            "NARP" },
  { PROTO_IPV6ICMP,        "ICMPv6" },
  { PROTO_IPV6NONEXT,      "IPv6 no next header" },
  { PROTO_IPV6OPTS,        "IPv6 options" },
  { PROTO_RSPF,            "Radio shortest path first" },
  { PROTO_VMTP,            "Versatile message transport" },
  { PROTO_OSPF,            "OSPF" },
  { PROTO_IPIP,            "IPinIP" },
  { PROTO_ENCAP,           "IPinIP encapsulation" },
  { 0, "" }
};

