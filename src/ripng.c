/**************************************************************************** 
** File: ripng.c
**
** Author: Mike Borella
**
** Comments: Dump RIPng header information. 
**
** $Id: ripng.c,v 1.10 2006/11/21 07:47:34 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "ripng.h"

#define HOLDER_SIZE 64

/*
 * RIPng command map
 */

strmap_t ripng_command_map [] =
{
  { RIPNG_CMD_RQ,          "request" },
  { RIPNG_CMD_RP,          "reply" },
  { 0, "" }
};

extern struct arg_t * my_args;

/*----------------------------------------------------------------------------
**
** dump_ripng()
**
** Parse RIPng packet and dump fields
**
**----------------------------------------------------------------------------
*/

void dump_ripng(packet_t *pkt)
{
  ripng_header_t       ripng;
  ripng_route_header_t ripng_route;
  char                 holder[HOLDER_SIZE];
  int                  route_count;

  /* Set the layer */
  set_layer(LAYER_APPLICATION);

  /*
   * Get the RIPng static header
   */
      
  if (get_packet_bytes((u_int8_t *) &ripng, pkt, sizeof(ripng_header_t)) == 0)
    return;

  /*
   * Dump header
   */
      
  if (my_args->m)
    {
      display_minimal_string("| RIPng ");
      display_minimal_string(map2str(ripng_command_map, ripng.command));
      display_minimal_string(" ");
    }
      else
    {
      /* announcement */
      display_header_banner("RIPng Header");
      
      snprintf(holder, HOLDER_SIZE, "%d (%s)", ripng.command, 
	       map2str(ripng_command_map, ripng.command));
      display_string("Command", holder);
      display("Version", (u_int8_t *) &ripng.version, 1, DISP_DEC);
      display("MBZ", (u_int8_t *) &ripng.mbz, 2, DISP_DEC);
    }
      
  /*
   * Do the individual routes.
   */
  
  route_count = 0;
  while (1)
    {
      if (get_packet_bytes((u_int8_t *) &ripng_route, pkt, 
			   sizeof(ripng_route_header_t)) == 0)
	break;
	      
      /*
       * Conversions
       */
      
      ripng_route.route_tag = ntohs(ripng_route.route_tag);

      /*
       * Dump route header
       */
      
      if (my_args->m)
	{
	  /* just count routes in minimal mode */
	  route_count++;
	}
      else
	{
	  display_ipv6("Address", (u_int8_t *) &ripng_route.address);
	  display("Route tag", (u_int8_t *) &ripng_route.route_tag, 2, 
		  DISP_DEC);
	  display("Netmask", (u_int8_t *) &ripng_route.netmask, 1, 
		  DISP_DEC);
	  display("Metric", (u_int8_t *) &ripng_route.metric, 1, 
		  DISP_DEC);
	}
      
    } /* while */

  /* display route count in minimal mode */
  if (my_args->m)
    {
      display_minimal((u_int8_t *) &route_count, 4, DISP_DEC);
      display_minimal_string(" routes ");
    }
  
}

