/**************************************************************************** 
** File: rsvp.c
**
** Author: Mike Borella
**
** Comments: 
**
** $Id: rsvp.c,v 1.4 2007/06/25 12:20:59 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "rsvp.h"

extern struct arg_t *my_args;

/* 
 * Types of RSVP messages
 */

#define RSVP_MSG_PATH      1
#define RSVP_MSG_RESV      2
#define RSVP_MSG_PATHERR   3
#define RSVP_MSG_RESVERR   4
#define RSVP_MSG_PATHTEAR  5
#define RSVP_MSG_RESVTEAR  6
#define RSVP_MSG_RESVCONF  7

/* 
 * RSVP message type map
 */

strmap_t rsvp_msgtype_map [] =
{
  { RSVP_MSG_PATH,     "Path" },
  { RSVP_MSG_RESV,     "Resv" },
  { RSVP_MSG_PATHERR,  "PathErr" },
  { RSVP_MSG_RESVERR,  "ResvErr" },
  { RSVP_MSG_PATHTEAR, "PathTear" },
  { RSVP_MSG_RESVTEAR, "ResvTear" },
  { RSVP_MSG_RESVCONF, "ResvConf" }
};

/*----------------------------------------------------------------------------
**
** dump_rsvp()
**
** Displays RSVP packets
**
**----------------------------------------------------------------------------
*/

void dump_rsvp(packet_t *pkt)
{
  rsvp_header_t rsvp;
  u_int8_t version;
  u_int8_t flags;

  /* Set the layer */
  set_layer(LAYER_APPLICATION);

  /* Stats accounting */
  stats_update(STATS_IP);

  /* Get the common header */
  if (get_packet_bytes((u_int8_t *) &rsvp, pkt, sizeof(rsvp_header_t)) == 0)
    return;

  /* Conversions */
  version = rsvp.version;
  flags = rsvp.flags;
  rsvp.checksum = ntohs(rsvp.checksum);
  rsvp.length = ntohs(rsvp.length);

  /* Dump the fields */
  if (!my_args->m)
    {
      display_header_banner("RSVP Header");
      
      display("Version", (u_int8_t *) &version, 1, DISP_DEC);
      display("Flags", (u_int8_t *) &flags, 1, DISP_HEX);
      display_strmap("Message type", rsvp.msg_type, rsvp_msgtype_map);
      display("Checksum", (u_int8_t *) &rsvp.checksum, 2, DISP_DEC);
      display("Send TTL", (u_int8_t *) &rsvp.send_ttl, 1, DISP_DEC);
      display("Reserved", (u_int8_t *) &rsvp.reserved, 1, DISP_DEC);
      display("Length", (u_int8_t *) &rsvp.length, 2, DISP_DEC);
    }
  else
    {
      /* Something for the minimal mode as well. */
      
      display_minimal_string ( " |RSVPv" );
      display_minimal ( (u_int8_t *) &version, 1, DISP_DEC );
      display_minimal_string ( " " );
      display_minimal_string ( map2str ( rsvp_msgtype_map, rsvp.msg_type ) );
    }
}
