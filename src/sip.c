/**************************************************************************** 
** File: sip.c
**
** Author: Mike Borella
**
** Comments: Dump SIP header information. I didn't try to do anything
** fancy with this - I just dump the plaintext headers.  This makes 
** debugging easier since parsing is such a pain to get right.
**
** $Id: sip.c,v 1.9 2001/05/28 19:24:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "sip.h"
#include "sdp.h"

#define LINE_SIZE 1500
#define FALSE 0
#define TRUE  1

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_sip()
**
** Parse SIP packet and dump fields. 
**
**----------------------------------------------------------------------------
*/

void dump_sip(packet_t *pkt)
{
  char *ptr;
  char line[LINE_SIZE];
  int use_sdp = FALSE;
  int i;
  int len;
  int seen_nonws;
  int content_length=0;

  /* Set the layer */
  set_layer(LAYER_APPLICATION);

  if (!my_args->m)
    {
      /* announcement */
      display_header_banner("SIP Headers");
      
      while(1)
	{
	  len = get_packet_line(line, LINE_SIZE, pkt);
	  if (len <= 0)
	    break;
	  
	  /*
	   * If we have a line of all whitespace, that's the end of the SIP
	   * headers.
	   */
	  
	  seen_nonws = 0;
	  for (i=0; i<strlen(line); i++)
	    {
	      if (!isspace(line[i]))
		{
		  seen_nonws = 1;
		  break;
		}
	    }
	  if (seen_nonws == 0)
	    break;
	  
	  /*
	   * Display a header
	   */
	  
	  display("Header", (u_int8_t *) line, strlen(line), 
		  DISP_STRING_MULTILINE);
	  
	  /*
	   * Convert each line to lower case for easier parsing.
	   */
	  
	  for (i=0; i<strlen(line); i++)
	    line[i] = tolower(line[i]);
	  
	  if (!my_args->m)
	    {    
	      if (!strncmp("content-type", line, 12))
		{
		  ptr = &line[14];
		  while(isspace(*ptr)) 
		    ptr++;
		  
		  if (!strncmp("application/sdp", ptr, 15))
		    use_sdp = TRUE;
		}
	      
	      /* Capture the content length so that we can pass it to SDP */
	      if (!strncmp("content-length", line, 14))
		{
		  char temp[32];
		  int  i = 0;
		  
		  /* skip whitespace */
		  ptr = &line[16];
		  while(isspace(*ptr)) 
		    ptr++;
		  
		  /* copy the numerals */
		  while(isdigit(*ptr))
		    {
		      temp[i] = *ptr;
		      i++;
		      ptr++;
		    }
		  temp[i] = '\0';
		  content_length = atoi(temp);
		}
	    }
	}
      
      /* dump the hexbuffer */
      hexbuffer_flush();
      
      if (use_sdp)
	dump_sdp(pkt, content_length);
    }
  else
    {
      /* Minimal mode processing. Just display the first line */
      display_minimal_string("SIP ");

      len = get_packet_line(line, LINE_SIZE, pkt);
      if (len > 0)
	{
	  display_minimal_string(line);
	  display_minimal_string(" ");
	}
    }
}

