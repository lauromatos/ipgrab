/**************************************************************************** 
** File: spx.c
**
** Author: Mike Borella
**
** Comments: Dump SPX header information
**
** $Id: spx.c,v 1.6 2001/05/28 19:24:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "spx.h"

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_spx()
**
** Parse SPX header and dump fields
**
**----------------------------------------------------------------------------
*/

void dump_spx(packet_t *pkt)
{
  spx_header_t spx;

  /* Set the layer */
  set_layer(LAYER_TRANSPORT);

  /*
   * Get the SPX header
   */

  if (get_packet_bytes((u_int8_t *) &spx, pkt, 12) == 0)
    return;

  /*
   * Conversions
   */

  spx.s_id = ntohs(spx.s_id);
  spx.d_id = ntohs(spx.d_id);
  spx.seqno = ntohs(spx.seqno);
  spx.ackno = ntohs(spx.ackno);
  spx.allocno = ntohs(spx.allocno);
  
  /*
   * Dump header
   */
 
  if (!my_args->n)
    {
      if (my_args->m)
	{
	  display_minimal_string("| SPX ");
	}
      else
	{
	  /* announcement */
	  display_header_banner("SPX Header");
	  
	  /* fields */
	  display("Connection control", (u_int8_t *) &spx.cc, 1, 
		  DISP_DEC);
	  display("Data stream type", (u_int8_t *) &spx.ds_type, 1, 
		  DISP_DEC);
	  display("Source conn ID", (u_int8_t *) &spx.s_id, 2, DISP_DEC);
	  display("Dest conn ID", (u_int8_t *) &spx.d_id, 2, DISP_DEC);
	  display("Sequence number", (u_int8_t *) &spx.seqno, 2, 
		  DISP_DEC);
	  display("Ack number", (u_int8_t *) &spx.ackno, 2, DISP_DEC);
	  display("Allocation number", (u_int8_t *) &spx.allocno, 2, 
		  DISP_DEC);
	}
    }

}
