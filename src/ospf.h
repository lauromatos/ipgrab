/**************************************************************************** 
** File: ospf.h
**
** Author: Mike Borella
**
** Comments: OSPF common header format
**
** $Id: ospf.h,v 1.5 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef OSPF_H
#define OSPF_H

#include "global.h"
#include "local.h"

/*
 * Format of OSPF common header 
 */

typedef struct ospf_common_header
{
  u_int8_t  version;
  u_int8_t  type;
  u_int16_t length;
  u_int32_t router_id;
  u_int32_t area_id;
  u_int16_t checksum;
  u_int16_t auth_type;
  u_int8_t  auth_data[8];
} ospf_common_header_t;

/*
 * Format of OSPF hello header 
 */

typedef struct ospf_hello_header
{
  u_int32_t netmask;
  u_int16_t interval;
  u_int8_t  options;
  u_int8_t  router_priority;
  u_int32_t dead_interval;
  u_int32_t designated_router;
  u_int32_t backup_router;
} ospf_hello_header_t;

/*
 * OSPF types
 */

#define OSPF_TYPE_HELLO        1
#define OSPF_TYPE_DATABASEDESC 2
#define OSPF_TYPE_LSREQUEST    3
#define OSPF_TYPE_LSUPDATE     4
#define OSPF_TYPE_LSACK        5

/*
 * OSPF Authentication types
 */

#define OSPF_AUTH_NONE         0
#define OSPF_AUTH_SIMPLE       1
#define OSPF_AUTH_CRYPTO       2

void dump_ospf(packet_t *);

#endif
