/**************************************************************************** 
** File: layers.h
**
** Author: Mike Borella
**
** Comments: Module that keeps track of what layer of the packet is 
**           being processed.
**
** $Id: layers.h,v 1.2 2001/05/28 19:24:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef LAYERS_H
#define LAYERS_H

#include "global.h"
#include "local.h"

typedef enum layer
{
  LAYER_NONE,        /* default for when a layer is not set */
  LAYER_DATALINK,
  LAYER_NETWORK,
  LAYER_TRANSPORT,
  LAYER_APPLICATION
} layer_t;

inline void    set_layer(layer_t);
inline layer_t get_layer(void);
inline int     check_layer(void);

#endif
