/**************************************************************************** 
** File: utilities.h
**
** Author: Mike Borella
**
** Comments: Utility functions for general use
**
** $Id: utilities.h,v 1.9 2006/11/21 07:47:35 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef UTILITIES_H
#define UTILITIES_H

#include "global.h"

inline void   reverse_byte_order(u_int8_t *, int);
inline int    isprint_str(char *, int);
inline int    isspace_str(char *, int);
inline char * argv2str(char **);
inline void * my_malloc(size_t);
inline void   my_free(void *);
inline void   dump_hex_and_text(char *, int);
inline int    ipaddr_space(u_int32_t);
inline char * make_space_str(int);

#endif
