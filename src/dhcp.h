/**************************************************************************** 
** File: dhcp.h
**
** Author: Mike Borella
**
** Comments: Structure of DHCP packets
**
** $Id: dhcp.h,v 1.8 2007/06/25 13:00:38 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef DHCP_H
#define DHCP_H

#include "global.h"
#include "local.h"

#define DHCP_OP_BOOTREQUEST 1
#define DHCP_OP_BOOTREPLY  2

#define BOOTP_COOKIE 0x63825363


#define DHCP_OPTION_PAD              0
#define DHCP_OPTION_NETMASK          1
#define DHCP_OPTION_TIMEOFFSET       2
#define DHCP_OPTION_ROUTER           3
#define DHCP_OPTION_TIMESERVER       4
#define DHCP_OPTION_NAMESERVER       5
#define DHCP_OPTION_DNS              6
#define DHCP_OPTION_LOGSERVER        7
#define DHCP_OPTION_COOKIESERVER     8
#define DHCP_OPTION_LPRSERVER        9
#define DHCP_OPTION_IMPRESSSERVER    10
#define DHCP_OPTION_RESLOCSERVER     11
#define DHCP_OPTION_HOSTNAME         12
#define DHCP_OPTION_BOOTFILESIZE     13
#define DHCP_OPTION_MERITDUMP        14
#define DHCP_OPTION_DOMAINNAME       15
#define DHCP_OPTION_SWAPSERVER       16
#define DHCP_OPTION_ROOTPATH         17
#define DHCP_OPTION_EXTSPATH         18
#define DHCP_OPTION_IPFORWARD        19
#define DHCP_OPTION_NONLOCALSR       20
#define DHCP_OPTION_POLICYFILTER     21
#define DHCP_OPTION_MAXREASSEMBLE    22
#define DHCP_OPTION_IPTTL            23
#define DHCP_OPTION_PATHMTUAGING     24
#define DHCP_OPTION_PATHMTUPLATEAU   25
#define DHCP_OPTION_INTERFACEMTU     26
#define DHCP_OPTION_SUBNETSLOCAL     27
#define DHCP_OPTION_BCASTADDRESS     28
#define DHCP_OPTION_MASKDISCOVERY    29
#define DHCP_OPTION_MASKSUPPLIER     30
#define DHCP_OPTION_ROUTERDISCOVERY  31
#define DHCP_OPTION_ROUTERSOLIC      32
#define DHCP_OPTION_STATICROUTE      33
#define DHCP_OPTION_TRAILERENCAPS    34
#define DHCP_OPTION_ARPTIMEOUT       35
#define DHCP_OPTION_ETHERNETENCAPS   36
#define DHCP_OPTION_TCPTTL           37
#define DHCP_OPTION_TCPKEEPALIVEINT  38
#define DHCP_OPTION_TCPKEEPALIVEGRBG 39
#define DHCP_OPTION_NISDOMAIN        40
#define DHCP_OPTION_NISSERVERS       41
#define DHCP_OPTION_NTPSERVERS       42
#define DHCP_OPTION_VENDORSPECIFIC   43
#define DHCP_OPTION_NETBIOSNAMESERV  44
#define DHCP_OPTION_NETBIOSDGDIST    45
#define DHCP_OPTION_NETBIOSNODETYPE  46
#define DHCP_OPTION_NETBIOSSCOPE     47
#define DHCP_OPTION_X11FONTS         48
#define DHCP_OPTION_X11DISPLAYMNGR   49
#define DHCP_OPTION_REQUESTEDIPADDR  50
#define DHCP_OPTION_IPADDRLEASE      51
#define DHCP_OPTION_OVERLOAD         52
#define DHCP_OPTION_MESSAGETYPE      53
#define DHCP_OPTION_SERVERID         54
#define DHCP_OPTION_PARAMREQLIST     55
#define DHCP_OPTION_MESSAGE          56
#define DHCP_OPTION_MAXDHCPMSGSIZE   57
#define DHCP_OPTION_RENEWALTIME      58
#define DHCP_OPTION_REBINDINGTIME    59
#define DHCP_OPTION_VENDORCLASSID    60
#define DHCP_OPTION_CLIENTID         61
#define DHCP_OPTION_NISPLUSDOMAIN    64
#define DHCP_OPTION_NISPLUSSERVERS   65
#define DHCP_OPTION_TFTPSERVER       66
#define DHCP_OPTION_BOOTFILE         67
#define DHCP_OPTION_MOBILEIPHOME     68
#define DHCP_OPTION_SMTPSERVER       69
#define DHCP_OPTION_POP3SERVER       70
#define DHCP_OPTION_NNTPSERVER       71
#define DHCP_OPTION_WWWSERVER        72
#define DHCP_OPTION_FINGERSERVER     73
#define DHCP_OPTION_IRCSERVER        74
#define DHCP_OPTION_STSERVER         75
#define DHCP_OPTION_STDASERVER       76
#define DHCP_OPTION_USERCLASS        77
#define DHCP_OPTION_SLPDIRAGENT      78
#define DHCP_OPTION_END              255

/*
 * DHCP message types
 */

#define DHCP_MSGTYPE_DISCOVER   1
#define DHCP_MSGTYPE_OFFER      2
#define DHCP_MSGTYPE_REQUEST    3
#define DHCP_MSGTYPE_DECLINE    4
#define DHCP_MSGTYPE_ACK        5
#define DHCP_MSGTYPE_NAK        6
#define DHCP_MSGTYPE_RELEASE    7
#define DHCP_MSGTYPE_INFORM     8

/*
 * DHCP header
 */

typedef struct dhcp_header
{

/* No need for this

#if defined(WORDS_BIGENDIAN)
  u_int8_t hops;
  u_int8_t hlen;
  u_int8_t htype;
  u_int8_t op;
#else
*/

  u_int8_t op;
  u_int8_t htype;
  u_int8_t hlen;
  u_int8_t hops;

/*
#endif
*/

  u_int32_t xid;
  u_int16_t secs;
  u_int16_t flags;
  u_int32_t ciaddr;
  u_int32_t yiaddr;
  u_int32_t siaddr;
  u_int32_t giaddr;
  char chaddr [16];
  char sname [64];
  char file [128];
} dhcp_header_t;

void dump_dhcp(packet_t *);

#endif
