/**************************************************************************** 
** File: ah.c
**
** Author: Mike Borella
**
** Dump AH payload
**
** $Id: ah.c,v 1.10 2002/01/03 00:04:01 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "ah.h"
#include "ip_protocols.h"

#define HOLDER_SIZE 256

extern struct arg_t *my_args;
extern strmap_t ipproto_map[];

extern void (*ip_proto_func[])(packet_t *);

/*----------------------------------------------------------------------------
**
** dump_ah()
**
** Parse AH packet and dump fields.
**
**----------------------------------------------------------------------------
*/

void dump_ah(packet_t *pkt)
{
  ah_header_t ah;
  char        holder[HOLDER_SIZE];
  u_int16_t   ad_length;
  u_int8_t    len;
  u_int8_t    next;
  u_int16_t   reserved;

  /* Set the layer */
  set_layer(LAYER_NETWORK);

  /*
   * Stats accounting
   */

  stats_update(STATS_AH);

  /*
   * Get the AH header
   */

  if (get_packet_bytes((u_int8_t *) &ah, pkt, 12) == 0)
    return;

  /*
   * Conversions
   */

  len = ah.length;
  next = ah.next;
  reserved = ah.reserved;
  ah.spi = ntohl(ah.spi);
  ah.seqno = ntohl(ah.seqno);

  /*
   * Figure out length of authentication data, hoping its not too big...
   */

  ad_length = len * 4 - 12;
  if (get_packet_bytes((u_int8_t *) &holder, pkt, ad_length) == 0)
    return;
  

  /*
   * Dump header
   */

  if (my_args->m && !my_args->n)
    {
      display_minimal_string("| AH ");
      display_minimal((u_int8_t *) &ah.spi, 4, DISP_HEX); 
      display_minimal_string(" ");
    }
  else
    if (!my_args->n)
      {
        /* announcement */
        display_header_banner("Authentication Header");
        
        /* print fields */
        snprintf(holder, HOLDER_SIZE, "%d (%s)", next, 
		 map2str(ipproto_map, next));
        display("Next header", holder, strlen(holder), DISP_STRING);
        display("Reseerved", (u_int8_t *) &reserved, 2, DISP_DEC);
        display("Header length", (u_int8_t *) &len, 1, DISP_DEC);
	display("SPI", (u_int8_t *) &ah.spi, 4, DISP_HEX); 
        display("Sequence number", (u_int8_t *) &ah.seqno, 4, DISP_DEC);
      }
	
  /*
   * Figure out the next header
   */
  
  if (ip_proto_func[ah.next])
    ip_proto_func[ah.next](pkt);
  
}
