/**************************************************************************** 
** File: ip_services.c
**
** Author: Mike Borella
**
** Comments: Functions to handle certain IP protocol numbers
**
** $Id: ip_services.c,v 1.14 2006/11/21 07:47:34 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "ip_services.h"
#include "dynports.h"

/*
 * We'll put the port map here even though it is used by TCP and UDP.
 * BTW, we can call these anything we'd like.  Suggestions welcome :-)
 */

strmap_t port_map[] = 
{
  { PORT_TCPMUX,           "TCP multiplexing" },
  { PORT_ECHO,             "echo" },
  { PORT_DISCARD,          "discard" },
  { PORT_SYSTAT,           "systat" },
  { PORT_DAYTIME,          "daytime" },
  { PORT_NETSTAT,          "netstat" },
  { PORT_QOTD,             "quote of the day" },
  { PORT_MSP,              "MSP" },
  { PORT_CHARGEN,          "character generator" },
  { PORT_FTPDATA,          "FTP data" },
  { PORT_FTPCTRL,          "FTP control" },
  { PORT_SSH,              "SSH" },
  { PORT_TELNET,           "telnet" },
  { PORT_SMTP,             "SMTP" },
  { PORT_TIME,             "time" },
  { PORT_RLP,              "RLP" },
  { PORT_NAMESERVER,       "name server" },
  { PORT_WHOIS,            "whois" },
  { PORT_REMOTEMAILCHECK,  "remote mail checking" },
  { PORT_DNS,              "DNS" },
  { PORT_MTP,              "MTP" },
  { PORT_DHCPSERVER,       "DHCP/BOOTP server" },
  { PORT_DHCPCLIENT,       "DHCP/BOOTP client" },
  { PORT_TFTP,             "TFTP" },
  { PORT_GOPHER,           "gopher" },
  { PORT_RJE,              "RJE" },
  { PORT_FINGER,           "finger" },
  { PORT_HTTP,             "http" },
  { PORT_TTYLINK,          "ttylink" },
  { PORT_KERBEROS,         "Kerberos" },
  { PORT_SUPDUP,           "SUPDUP" },
  { PORT_HOSTNAMES,        "hostnames" },
  { PORT_ISOTSAP,          "ISO TSAP" },
  { PORT_CSNETNAMESERVICE, "CSNET name service" },
  { PORT_EUDORA,           "Eudora" },
  { PORT_RTELNET,          "rtelnet" },
  { PORT_POP2,             "POP2" },
  { PORT_POP3,             "POP3" },
  { PORT_SUNRPC,           "sunrpc" },
  { PORT_NNTP,             "nntp" },
  { PORT_AUTH,             "auth" },
  { PORT_SNMP,             "SNMP" },

  { PORT_NETBIOSNS,        "NETBIOS name service" },
  { PORT_NETBIOSDGM,       "NETBIOS datagram service" },
  { PORT_NETBIOSSSN,       "NETBIOS session service" },

  { PORT_SLP,              "SLP" },
  { PORT_MOBILEIP,         "Mobile IP"},
  { PORT_ISAKMP,           "ISAKMP/IKE" },
  { PORT_RIP,              "RIP" },
  { PORT_RIPNG,            "RIPng" },
  { PORT_CDMA2000A11,      "CDMA2000 A11" },

  { PORT_L2TP,             "L2TP" },
  { PORT_PPTP,             "PPTP" },

  { PORT_RADIUS,           "RADIUS" },
  { PORT_RADACCOUNT,       "RADIUS accounting" },

  { PORT_SIP,              "SIP" },
  { 0, "" }
};


extern struct arg_t * my_args;

/*----------------------------------------------------------------------------
**
** port2func(u_int16_t)
**
** Map a port number to a function to call
**
**----------------------------------------------------------------------------
*/

service_func_t port2func(u_int16_t port)
{
  service_func_t f = NULL;

  /* Check for dynamic mappings */
  f = dynports_find(port);
  
  if (f == NULL)
    {
      switch(port)
	{
	case PORT_SSH:
	  f = dump_ssh;
	  break;
	  
	case PORT_SIP:
	  f = dump_sip;
	  break;
	  
	case PORT_DNS:
	  f = dump_dns;
	  break;
	  
	case PORT_DHCPCLIENT:
	case PORT_DHCPSERVER:
	  f = dump_dhcp;
	  break;
	  
	case PORT_HTTP:
	  f = dump_http;
	  break;

	case PORT_NNTP:
	  f = dump_nntp;
	  break;
      
	case PORT_NETBIOSNS:
	  f = dump_netbios_ns;
	  break;

	case PORT_SNMP:
	  f = dump_snmp;
	  break;
	  
	case PORT_SLP:
	  f = dump_slp;
	  break;
	  
	case PORT_MOBILEIP:
	  f = dump_mobileip;
	  break;
	  
	case PORT_ISAKMP:
	  f = dump_isakmp;
	  break;
      
	case PORT_RIP:
	  f = dump_rip;
	  break;
	  
	case PORT_RIPNG:
	  f = dump_ripng;
	  break;
	  
	case PORT_CDMA2000A11:
	  f = dump_mobileip;
	  break;
	  
	case PORT_L2TP:
	  f = dump_l2tp;
	  break;
	  
	case PORT_PPTP:
	  f = dump_pptp;
	  break;

	case PORT_FTPCTRL:
	  f = dump_ftpctrl;
	  break;

	case PORT_TFTP:
	  f = dump_tftp;
	  break;

	case PORT_RADIUS:
	case PORT_RADACCOUNT:
	  f = dump_radius;
	  break;

	default:
	  f = NULL;
	  break;
	}
    }

  return f;
}
