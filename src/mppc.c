/**************************************************************************** 
** File: mppc.c
**
** Author: Mike Borella
**
** Comments: MPPC/MPPE protocol module.  
**
** Microsoft's point to point compression and point to point encryption
** use the same formats so they are combined here.  Basically this code
** is never executed unless the user runs ipgrab with the "-C mppc" option.
** There is no way to know that a compressed frame is MPPC encoded, so
** we need a hint from the user.
**
** $Id: mppc.c,v 1.1 2001/10/02 22:29:01 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "mppc.h"

extern struct arg_t * my_args;

/*----------------------------------------------------------------------------
**
** dump_mppc()
**
** Parse and display MPPC and MPPE packets.  Basically we can only decode
** two bytes.  The first nibble contains the A, B, C, and D bits, and the 
** second third and fourth nibbles contain the coherency count.
** 
**----------------------------------------------------------------------------
*/

void dump_mppc(packet_t *pkt)
{
  u_int16_t  bytes;
  u_int8_t   a_bit;
  u_int8_t   b_bit;
  u_int8_t   c_bit;
  u_int8_t   d_bit;
  u_int16_t  coherency;

  /* Set the layer */
  set_layer(LAYER_DATALINK);

  /* get the bytes */
  if (get_packet_bytes((u_int8_t *) &bytes, pkt, 2) == 0)
    return;

  /* conversions */
  bytes = ntohs(bytes);
  a_bit = (bytes & 0x8000) >> 15;
  b_bit = (bytes & 0x4000) >> 14;
  c_bit = (bytes & 0x2000) >> 13;
  d_bit = (bytes & 0x1000) >> 12;
  coherency = bytes & 0x0fff;

  /* Figure out what to print */
  if (my_args->m)
    {
    }
  else
    {
      /* announcement */
      display_header_banner("MPPC Header");

      display ("A bit (flushed)", &a_bit, 1, DISP_DEC);
      display ("B bit (reset history)", &b_bit, 1, DISP_DEC);
      display ("C bit (compressed)", &c_bit, 1, DISP_DEC);
      display ("D bit (encrypted)", &d_bit, 1, DISP_DEC);
      display ("Coherency count", (u_int8_t *) &coherency, 2, DISP_DEC);
    }

  /* dump the hex buffer */
  hexbuffer_flush();

}
