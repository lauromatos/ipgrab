/**************************************************************************** 
** File: mgcp.c
**
** Author: Cullen Jennings & Mike Borella
**
** Comments: Dump MGCP header information. I didn't try to do anything
** fancy with this - I just dump the plaintext 
**
** $Id: mgcp.c,v 1.6 2000/08/30 20:23:04 mborella Exp $

** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "mgcp.h"
#include "sdp.h"

#define LINE_SIZE 256
#define FALSE     0
#define TRUE      1

/*----------------------------------------------------------------------------
**
** is_mgcp()
**
** Return true if this udp packet is a MGCP packet 
**
**----------------------------------------------------------------------------
*/

int is_mgcp(packet_t *pkt)
{
   if ( 0 < 4 ) return 0; /* FIXME!!*/

   if ( !strncmp(pkt->current,"EPCF",4) ) return 1;
   if ( !strncmp(pkt->current,"RQNT",4) ) return 1;
   if ( !strncmp(pkt->current,"NTFY",4) ) return 1;
   if ( !strncmp(pkt->current,"CRCX",4) ) return 1;
   if ( !strncmp(pkt->current,"MDCX",4) ) return 1;
   if ( !strncmp(pkt->current,"DLCX",4) ) return 1;
   if ( !strncmp(pkt->current,"AUEP",4) ) return 1;
   if ( !strncmp(pkt->current,"AUCX",4) ) return 1;
   if ( !strncmp(pkt->current,"RSIP",4) ) return 1;

   return 0;
}


/*----------------------------------------------------------------------------
**
** dump_mgcp()
**
** Parse MGCP packet and dump fields. 
**
**----------------------------------------------------------------------------
*/

void dump_mgcp(packet_t *pkt)
{
  /*  char line[LINE_SIZE];
      int n = 0;*/

  printf("-----------------------------------------------------------------\n");
  printf("                        MGCP \n");
  printf("-----------------------------------------------------------------\n");

  /* FIX ME LATER 
  while(p <= ep && (n = get_next_line(p, ep, line)))
    {
      p = p + n;
      length = length - n;

      printf("%s\n", line);
    }
  */
}

