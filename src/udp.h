/**************************************************************************** 
** File: udp.h
**
** Author: Mike Borella
**
** Comments: Generic UDP header structure - an attempt at OS independence
**
** $Id: udp.h,v 1.5 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef UDP_H
#define UDP_H

#include "global.h"
#include "local.h"

typedef struct udp_header
{
  u_int16_t src;
  u_int16_t dst;
  u_int16_t length;
  u_int16_t checksum;
} udp_header_t;

void dump_udp(packet_t *);

#endif
