/**************************************************************************** 
** File: padding.c
**
** Author: Mike Borella
**
** Comments: Dump packet padding
**
** $Id: padding.c,v 1.2 2001/09/07 22:50:31 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "padding.h"

#define PADDING_SIZE  1500

/*----------------------------------------------------------------------------
**
** dump_padding()
**
** Dump printable portions of packet padding
**
**----------------------------------------------------------------------------
*/

void dump_padding(packet_t *pkt)
{
  char   holder[PADDING_SIZE];
  int    bytes;

  /* Set the layer */
  set_layer(LAYER_APPLICATION);

  /* display announcement */
  display_header_banner("Padding");

  /* The rest of the packet is padding, so lets grab the whole thing */
  skip_packet_toapparentend(pkt);
  bytes = get_packet_bytesleft(pkt);
  if (bytes <= 0) 
    return;
  if (get_packet_bytestoend(holder, pkt, bytes) == 0)
    return;
  
  /*
   * Display the hex and text
   */

  dump_hex_and_text(holder, bytes);

  return;
}
