/**************************************************************************** 
**
** File: tcp.h
**
** Author: Mike Borella
**
** Comments: Generic TCP header structure - an attempt at OS independence
**
** $Id: tcp.h,v 1.6 2000/08/30 20:23:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef TCP_H
#define TCP_H

#include "global.h"
#include "local.h"

/*
 * TCP flags 
 */

#define TCP_FLAG_FIN  0x01
#define TCP_FLAG_SYN  0x02
#define TCP_FLAG_RST  0x04
#define TCP_FLAG_PSH  0x08
#define TCP_FLAG_ACK  0x10
#define TCP_FLAG_URG  0x20

/*
 * TCP options 
 */

#define TCP_OPTION_EOL    0
#define TCP_OPTION_NOP    1
#define TCP_OPTION_MSS    2
#define TCP_OPTION_WS     3
#define TCP_OPTION_SACKOK 4
#define TCP_OPTION_SACK   5
#define TCP_OPTION_TS     8 

/*
 * TCP header
 */

typedef struct tcp_header
{
  u_int16_t src;                    /* source port */
  u_int16_t dst;                    /* destination port */
  u_int32_t seq_number;             /* sequence number */
  u_int32_t ack_number;             /* acknowledgement number */
#ifdef WORDS_BIGENDIAN
  u_int8_t  header_length:4,        /* data offset */
            unused:4;               /* (unused) */
#else
  u_int8_t  unused:4,               /* (unused) */
            header_length:4;        /* data offset */
#endif
  u_int8_t  flags;
  u_int16_t window;                 /* window */
  u_int16_t checksum;               /* checksum */
  u_int16_t urgent;                 /* urgent pointer */
} tcp_header_t;

void dump_tcp(packet_t *);

#endif 
