/**************************************************************************** 
**
** File: open_pcap.h
**
** Author: Mike Borella
**
** Comments: Set up pcap to sniff the packets we want.  Most of these 
** commands are listed in the pcap(3) man page.
**
** $Id: open_pcap.h,v 1.2 2000/08/29 21:59:02 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef OPEN_PCAP_H
#define OPEN_PCAP_H

#include "global.h"

int open_pcap(void);

#endif
