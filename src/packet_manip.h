/**************************************************************************** 
** File: packet_manip.h
**
** Author: Mike Borella
**
** Comments: Header file for packet structures and manipulation functions
**
** The structure used here gives us pointers to several important areas
** in a packet.
** 
** contents: points to the first byte of the packet.
** current: points to the next unconsumed byte.
** end: points to the real end of the packet.
** apparent_end: points to where IP (or another protocol) thinks the 
**               packet should end (this value is not accurate until IP
**               IP or some other protocol processes the packet).
** mark: points to a user defined mark (only used for DNS right now...
** 
**          +++++++++++++++++++++++++++++++++++++++++++++++
**          ^          ^                      ^           ^
**          |          |                      |           |
**      contents    current             apparent_end     end
**
** Note that the difference between the end and the apparent_end is due
** to padding, typically done by a link layer device.  In most cases, there
** is no padding and apparent_end == end.
**
** $Id: packet_manip.h,v 1.11 2001/10/09 23:20:42 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef PACKET_MANIP_H
#define PACKET_MANIP_H

#include "global.h"

#define PACKET_TIMESTAMP_LEN 32

/*
 * Data structure that contains the bytestring of a packet and pointers 
 * to the next byte to read as well as the end.
 */

typedef struct packet
{
  u_int8_t * contents;
  u_int8_t * current;
  u_int8_t * end;
  u_int8_t * apparent_end; /* where a protocol such as IP thinks the end is */
  u_int8_t * mark;
  u_int32_t  media_length; /* length of the packet as reported by the media */
  char       timestamp [PACKET_TIMESTAMP_LEN];
} packet_t;


inline int       look_packet_bytes(u_int8_t *, packet_t *, unsigned int);
inline int       get_packet_bytes(u_int8_t *, packet_t *, unsigned int);
inline int       get_packet_bytestoend(u_int8_t *, packet_t *, unsigned int);
inline int       get_packet_line(u_int8_t *, u_int32_t, packet_t *);
inline int       get_packet_string(u_int8_t *, u_int32_t, packet_t *);
inline int       skip_packet_bytes(packet_t *, unsigned int);
inline int       skip_packet_toapparentend(packet_t *);
inline u_int32_t get_packet_bytesleft(packet_t *);
inline u_int32_t get_packet_apparentbytesleft(packet_t *);
inline void      set_packet_mark(packet_t *);
inline int32_t   get_packet_markdistance(packet_t *);
inline void      set_packet_apparentend(packet_t *, int);
inline int       packet_haspadding(packet_t *);

#endif
