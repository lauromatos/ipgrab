/**************************************************************************** 
**
** File: dynports.h
**
** Author: Mike Borella
**
** Header Definitions for dynamic port mapping
**
** $Id: dynports.h,v 1.3 2001/10/11 20:43:50 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#ifndef DYNPORTS_H
#define DYNPORTS_H

#include "global.h"
#include "ip_services.h"

/*
 * Function prototypes
 */

void           dynports_parse(char *);
service_func_t dynports_find(u_int16_t);
void           dynports_add(u_int16_t, service_func_t, unsigned long);
void           dynports_refresh(u_int16_t, service_func_t);
void           dynports_timeout(void);

#endif
