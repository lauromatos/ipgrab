/**************************************************************************** 
** File: nntp.c
**
** Author: Mike Borella
**
** Comments: NNTP protocol module.  
**
** Since NNTP is both stateful and ASCII, there isn't much "parsing" to 
** do.  Instead we grab each string and try to display as much as makes 
** sense.  This is NNTP_MINMODE_LEN characters for minimal mode, but the 
** whole thing for verbose mode.
**
** $Id: nntp.c,v 1.1 2001/11/15 00:22:36 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "nntp.h"

#define NNTP_MINMODE_LEN 50 /* The length of printed strings in minimal mode */

extern struct arg_t * my_args;

/*----------------------------------------------------------------------------
**
** dump_nntp()
**
** Parse and display NNTP packets
**
**----------------------------------------------------------------------------
*/

void dump_nntp(packet_t *pkt)
{
  u_int8_t * payload;
  u_int32_t  size;
  int        i;

  /* Set the layer */
  set_layer(LAYER_APPLICATION);

  /* find the paylaod size, then allocate memory and read the packet */
  size = get_packet_apparentbytesleft(pkt);
  if (size <= 0)
    return;
  payload = (u_int8_t *) my_malloc (size+1);
  payload[size] = '\0';
  if (get_packet_bytes(payload, pkt, size) == 0)
    {
      my_free(payload);
      return;
    } 

  /* Get rid of those pesky \r's, \t's and \n's, replace them with spaces */
  for (i=0; i<size; i++)
    {
      if (payload[i] == '\r' || payload[i] == '\t' || payload[i] == '\n')
	payload[i] = ' ';
    }

  /* Display */
  if (my_args->m)
    {
      display_minimal_string("| NNTP ");
      if (size > NNTP_MINMODE_LEN)
	payload[NNTP_MINMODE_LEN] ='\0';
      display_minimal_string(payload);
    }
  else
    {
      display_header_banner("NNTP");
      display("Data", payload, size, DISP_STRING_MULTILINE);
    }
  
  /* free memory, of course */
  my_free(payload);
  
  /* dump the hex buffer */
  hexbuffer_flush();

}
