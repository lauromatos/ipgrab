/**************************************************************************** 
** File: slip.c
**
** Author: Michael S. Borella
**
** Comments: Dump slip packets
**
** $Id: slip.c,v 1.5 2001/05/28 19:24:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "slip.h"
#include "ip.h"

#define SLIP_HDRLEN 16

extern struct arg_t * my_args;

/*----------------------------------------------------------------------------
**
** dump_slip()
**
** Process packets from the DLT_RAW interface type
**
**----------------------------------------------------------------------------
*/

void dump_slip(packet_t *pkt)
{
  u_int8_t slip_header[SLIP_HDRLEN];
  
  /* Set the layer */
  set_layer(LAYER_DATALINK);

  /*
   * Get slip header
   */

  if (get_packet_bytes(slip_header, pkt, SLIP_HDRLEN) == 0)
    return;

  /* 
   * Dump headers 
   */

  if (my_args->m)
    {
      if (my_args->T)
	display_minimal_string("SLIP ");
      else
	display_minimal_string("| SLIP ");
    }
  else
    {
      /*
       * Dump header announcement
       */
      
      display_header_banner_ts("Slip", pkt->timestamp);
    }

  /* 
   * Dump the IP Packet 
   */

  dump_ip(pkt);
}
