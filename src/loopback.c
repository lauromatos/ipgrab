/**************************************************************************** 
**
** File: loopback.c
**
** Author: Mike Borella
**
** Comments: Dump loopback packets
**
** $Id: loopback.c,v 1.5 2001/05/28 19:24:04 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "loopback.h"
#include "ip_protocols.h"

#define LOOPBACK_FAMILY_INET 524288 /* magic cookie for IP over loopback */

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_loopback()
**
** Process packets from DLT_NULL device
**
**----------------------------------------------------------------------------
*/

void dump_loopback(packet_t *pkt)
{
  u_int32_t family;

  if (get_packet_bytes((u_int8_t *) &family, pkt, 4) == 0)
    return;

  /* Set the layer */
  set_layer(LAYER_DATALINK);

  /* 
   * Dump header 
   */

  if (my_args->m)
    {
      if (my_args->T)
	display_minimal_string("LOOPBACK ");
      else
	display_minimal_string("| LOOPBACK ");
    }
  else
    {
      /*
       * Dump header announcement
       */
      
      display_header_banner_ts("Loopback", pkt->timestamp);
      
      /*
       * Dump header field
       */
      
      display("Address family", (u_int8_t *) &family, 4, DISP_HEX);
    }
  
  switch (family)
    {
    case LOOPBACK_FAMILY_INET:
      dump_ip(pkt);
      break;
    default:
      break;
    }

  return;

}

