/**************************************************************************** 
** File: esp.c
**
** Author: Mike Borella
**
** Comments: Dump ESP header information. Since this packet sniffer
** is stateless, we cannot determine any headers besides the SPI and the
** sequence number.  In order to figure out the padding length and next 
** payload headers we would need to know the authentication mechanism 
** (if any) used for this SA.  But that info is part of the ISAKMP
** negotiation rather than the ESP header.  Oh well.
**
** $Id: esp.c,v 1.7 2006/11/21 07:47:34 farooq-i-azam Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "global.h"
#include "ip_protocols.h"

extern struct arg_t *my_args;

/*----------------------------------------------------------------------------
**
** dump_esp()
**
** Parse ESP packet and dump fields.
**
**----------------------------------------------------------------------------
*/

void dump_esp(packet_t *pkt)
{
  esp_header_t esp;

  /* Set the layer */
  set_layer(LAYER_NETWORK);

  /*
   * Stats accounting
   */

  stats_update(STATS_ESP);

  /*
   * Get the ESP header
   */

  if (get_packet_bytes((u_int8_t *) &esp, pkt, sizeof(esp_header_t)) == 0)
    return;

  /*
   * Conversions
   */

  esp.spi = ntohl(esp.spi);
  esp.spi = ntohl(esp.seqno);

  /*
   * Dump header
   */

  if (my_args->m)
    {
      display_minimal((u_int8_t *) &esp.spi, 4, DISP_HEX); 
      display_minimal_string(" ");
    }
  else
    {
      /* announcement */
      display_header_banner("ESP Header");
      display("SPI", (u_int8_t *) &esp.spi, 4, DISP_HEX); 
      display("Sequence number", (u_int8_t *) &esp.seqno, 4, DISP_DEC);
    }

}

