/******************************************************************************
**
** parse_cl.h
**
** Sun Oct  7 12:44:00 2001
** Linux 2.4.2 (#5 Thu Apr 26 12:29:16 CDT 2001) i686
** mborella@stratos.mw.3com.com (Mike Borella)
**
** Header file for command line parser
**
** Automatically created by genparse v0.5.2
**
** See http://genparse.sourceforge.net/ for details and updates
**
******************************************************************************/

#include <stdio.h>

#ifndef bool
typedef enum bool_t
{
  false = 0, true
} bool;
#endif

/* customized structure for command line parameters */
struct arg_t
{
  bool a;
  bool b;
  int c;
  char * C;
  bool d;
  char * i;
  bool l;
  bool m;
  bool n;
  char * P;
  bool p;
  char * r;
  bool T;
  bool t;
  char * w;
  bool x;
  bool h;
  bool v;
  int optind;
};

/* function prototypes */
struct arg_t * Cmdline(int, char **);
void usage(char *);
void free_args(struct arg_t *);

