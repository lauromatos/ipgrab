/**************************************************************************** 
** File: layers.c
**
** Author: Mike Borella
**
** Comments: 
**
** $Id: layers.c,v 1.3 2001/05/28 19:51:31 mborella Exp $
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU Library General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
**
*****************************************************************************/

#include "layers.h"

static layer_t l;
extern struct arg_t * my_args;

/*----------------------------------------------------------------------------
**
** set_layer()
**
** Set the layer.
**
**----------------------------------------------------------------------------
*/

inline void set_layer(layer_t new_layer)
{
  l = new_layer;
}

/*----------------------------------------------------------------------------
**
** get_layer()
**
** Get the layer.
**
**----------------------------------------------------------------------------
*/

inline layer_t get_layer(void)
{
  return l;
}

/*----------------------------------------------------------------------------
**
** check_layer()
**
** Checks the current layer against the command line args.  Returns 1 if 
** we should not print, 0 if we should.
**
**----------------------------------------------------------------------------
*/

inline int check_layer(void)
{
  switch(l)
    {
    case LAYER_NONE:
      return 0;

    case LAYER_DATALINK:
      if (my_args->l)
	return 1;
      else
	return 0;

    case LAYER_NETWORK:
      if (my_args->n)
	return 1;
      else
	return 0;

    case LAYER_TRANSPORT:
      if (my_args->t)
	return 1;
      else
	return 0;
                  
    case LAYER_APPLICATION:
      if (my_args->a)
	return 1;
      else
	return 0;

    default:
      error_fatal("unknown layer type");
      return 0;
    }
}
