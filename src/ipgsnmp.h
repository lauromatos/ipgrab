/* ipgsnmp.h
 * $Id: ipgsnmp.h,v 1.2 2000/08/30 20:23:04 mborella Exp $
 * Glen Wiley <gwiley@ieee.org>
 *
 * this is the stuff we need to expose in order to be able to decompose
 * snmp v1
 *
 * NOTE: I am relying on the ucd snmp library for much of the heavy 
 * lifting
 */

#include "global.h"
#include "local.h"

void dump_snmp(packet_t *);

/* ipgsnmp.h */
